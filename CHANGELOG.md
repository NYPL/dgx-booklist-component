## Changelog

### v0.2.1
- Making book-cover component http instead of ssh.

### v0.2.0
- Updating to React 15.

### v0.1.3
#### Added
- Added support for Google Analytics click events via `gaClickEvent` function property.
- Added `gaActionText` & `bookItem` property to generate a proper Google Analytics action text.
- Added README.md file to contain a CHANGELOG with detailed descriptions.

#### Changed
- Fixed ESLINT errors.
