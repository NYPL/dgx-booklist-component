import React from 'react';
import PropTypes from 'prop-types';

const NavButton = (props) => (
  <button
    className={props.className}
    data-role={props['data-role']}
    label={props.label}
    onClick={props.onClick}
    tabIndex="-1"
    aria-hidden
  >
    {props.label}
  </button>
);

NavButton.propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
  'data-role': PropTypes.string,
  onClick: PropTypes.func,
};

export default NavButton;
