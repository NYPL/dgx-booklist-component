import React from 'react';
import { render } from 'react-dom';
import BooklistWidget from './component.js';
import './styles/main.scss';

const dummyContent = {
  children: null,
  id: '',
  name: { en: { text: 'Staff Picks Carousel' } },
  type: 'container',
  slots: [
    {
      image: {
        bookCoverImage: {
          'full-uri': 'https://d7.nypl.org/sites/default/files/Chocky.jpg',
          description: 'This is the description for the book cover',
          alt: 'This is the alt for the book cover',
        },
      },
      link: 'http://browse.nypl.org/iii/encore/record/C__Rb20720345__SChocky%20__Orightresult' +
        '__U__X7?lang=eng&suite=def',
      title: { en: { text: 'Chocky' } },
      author: {
        fullName: 'John Wyndham',
      },
      bookItem: {
        audience: 'Adult',
        genre: 'Creepy',
      },
    },
    {
      image: {
        bookCoverImage: {
          'full-uri': 'https://d7.nypl.org/sites/default/files/Dietland.jpg',
          description: 'This is the description for the book cover',
          alt: 'This is the alt for the book cover',
        },
      },
      link: 'http://browse.nypl.org/iii/encore/record/C__Rb20617276__SDietland__Orightresult' +
        '__U__X7?lang=eng&suite=def',
      title: { en: { text: 'Dietland' } },
      author: {
        fullName: 'Sarai Walker',
      },
      bookItem: {
        audience: 'Young Adult',
        genre: 'Edgy',
      },
    },
    {
      image: {
        bookCoverImage: {
          'full-uri': 'https://d7.nypl.org/sites/default/files/Fifty_Mice.jpg',
          description: 'This is the description for the book cover',
          alt: 'This is the alt for the book cover',
        },
      },
      link: 'http://browse.nypl.org/iii/encore/record/C__Rb20333845__SFifty%20Mice__Oright' +
        'result__U__X7?lang=eng&suite=def',
      title: { en: { text: 'Fifty_Mice' } },
      author: {
        fullName: 'Daniel Pyne',
      },
      bookItem: {
        audience: 'Young Adult',
        genre: 'Otherworldy',
      },
    },
    {
      image: {
        bookCoverImage: {
          'full-uri': 'https://d7.nypl.org/sites/default/files/Furiously_Happy.jpg',
          description: 'This is the description for the book cover',
          alt: 'This is the alt for the book cover',
        },
      },
      link: 'http://browse.nypl.org/iii/encore/record/C__Rb20814005__SFuriously%20Happy%3A%' +
        '20a%20Funny%20Book%20About%20Horrible%20Things__Orightresult__U__X7?lang=eng&suite=def',
      title: { en: { text: 'Furiously_Happy' } },
      author: {
        fullName: 'Jenny Lawson',
      },
      bookItem: {
        audience: 'Children',
        genre: 'Funny',
      }
    },
    {
      image: {
        bookCoverImage: {
          'full-uri': 'https://d7.nypl.org/sites/default/files/Incognito.jpg',
          description: 'This is the description for the book cover',
          alt: 'This is the alt for the book cover',
        },
      },
      link: 'http://browse.nypl.org/iii/encore/record/C__Rb18403039__SIncognito__P0%2C1__' +
        'Orightresult__U__X7?lang=eng&suite=def',
      title: { en: { text: 'Incognito -- fake long title' } },
      author: {
        fullName: 'Ed Brubaker',
      },
      bookItem: {
        audience: 'Adult',
        genre: 'Historical',
      },
    },
  ],
};
const responsiveSettings = [
  {
    breakpoint: 1400,
    settings: {
      slidesToShow: 5,
    },
  },
  {
    breakpoint: 1230,
    settings: {
      slidesToShow: 4,
    },
  },
  {
    breakpoint: 1000,
    settings: {
      slidesToShow: 5,
    },
  },
  {
    breakpoint: 915,
    settings: {
      slidesToShow: 4,
    },
  },
  {
    breakpoint: 830,
    settings: {
      slidesToShow: 3,
    },
  },
  {
    breakpoint: 767,
    settings: {
      slidesToShow: 5,
    },
  },
  {
    breakpoint: 675,
    settings: {
      slidesToShow: 4,
    },
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 3,
    },
  },
  {
    breakpoint: 350,
    settings: {
      slidesToShow: 2,
    },
  },
];

// Used to mock gaClick event
const gaClickTest = () => (
  (action, label) => {
    console.log(action);
    console.log(label);
  }
);

const bookListWidget = (
  <BooklistWidget
    id="HP-Booklist"
    className="bookListWidget"
    bookLists={dummyContent.slots}
    gaClickEvent={gaClickTest()}
    gaActionText="New & Noteworthy"
  />
);

/* app.jsx
 * Used for local development of React Components
 */
render(bookListWidget, document.getElementById('booklist'));
