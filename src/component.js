import React from 'react';
import PropTypes from 'prop-types';

import Slider from 'react-slick';
import {
  isEmpty as _isEmpty,
} from 'underscore';

import BookCover from 'dgx-book-cover';
import NavButton from './components/NavButton.js';

const styles = {
  bookItemsWidth: {
    width: '4500px',
  },
};

class BooklistWidget extends React.Component {
  constructor(props) {
    super(props);

    this.generateItemsToDisplay = this.generateItemsToDisplay.bind(this);
  }

  generateItemsToDisplay(bookLists) {
    const bookCoverItems = (bookLists && bookLists.length) ?
      bookLists.map((element, i) => {
        const target = element.link;
        const imageContent = (element.image && element.image.bookCoverImage) ?
          element.image.bookCoverImage : {};
        const {
          'full-uri': fullUri = '',
          alt: alt = '',
        } = imageContent;
        const authorName = element.author ? element.author.fullName : '';
        const genre = element.bookItem && element.bookItem.genre ?
          element.bookItem.genre : undefined;
        const audience = element.bookItem && element.bookItem.audience ?
          element.bookItem.audience : undefined;

        return (
          <div key={i} className="slick-book-item">
            <BookCover
              id={`item-${i}`}
              imgSrc={fullUri}
              imgAlt={alt}
              target={target}
              title={element.title.en.text}
              author={authorName}
              genre={genre}
              audience={audience}
              className="bookItem"
              gaClickEvent={this.props.gaClickEvent}
              gaActionText={this.props.gaActionText}
              bookIndex={i}
            />
          </div>
        );
      })
      : null;

    if (bookLists && bookLists.length) {
      styles.bookItemsWidth.width = `${bookCoverItems.length * 149 - 29}px`;
    }

    return bookCoverItems;
  }

  render() {
    // Pass in model builder method here
    const bookLists = this.props.bookLists;
    const items = this.generateItemsToDisplay(bookLists);
    const responsiveSettings = this.props.slickResponsiveSettings;
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      arrows: true,
      nextArrow: <NavButton label="Next" />,
      prevArrow: <NavButton label="Previous" />,
      variableWidth: true,
      responsive: responsiveSettings,
    };

    // If the items were fetched and parsed but are empty
    // then, display appropriate error.
    if (!items || _isEmpty(items)) {
      return (
        <div className="errorMessage">{this.props.errorMessage}</div>
      );
    }

    // Items have been built, simply render.
    return (
      <Slider
        {...settings}
        className={this.props.className}
        id={this.props.id}
      >
        {items}
      </Slider>
    );
  }
}

BooklistWidget.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string,
  lang: PropTypes.string,
  bookLists: PropTypes.array,
  errorMessage: PropTypes.string,
  slickResponsiveSettings: PropTypes.array,
  gaClickEvent: PropTypes.func,
  gaActionText: PropTypes.string,
};

BooklistWidget.defaultProps = {
  id: 'BooklistWidget',
  className: 'BooklistWidget',
  lang: 'en',
  bookLists: [],
  errorMessage: 'We\'re sorry. Information isn\'t available for this feature.',
  slickResponsiveSettings: [],
};

export default BooklistWidget;
