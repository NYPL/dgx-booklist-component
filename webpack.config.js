var path = require('path');
var webpack = require('webpack');
var cleanBuild = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

// References the applications root path
var ROOT_PATH = path.resolve(__dirname);

if (process.env.NODE_ENV !== 'development') {
  module.exports = {
    devtool: 'source-map',
    entry: [
      path.resolve(ROOT_PATH, 'src/component.js'),
    ],
    resolve: {
      extensions: ['', '.js', '.jsx', '.scss'],
    },
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'index.min.js',
      // export itself to a UMD require library convention
      libraryTarget: 'umd',
      // name of the global var
      library: 'dgxBooklist',
    },
    externals: {
      // Required in order to ignore library within other components
      'react': {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react',
      },
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: ['babel'],
          query: {
            presets: ['es2015', 'react'],
          },
        },
        {
          test: /\.scss$/,
          include: path.resolve(ROOT_PATH, 'src'),
          loader: ExtractTextPlugin.extract(
            // activate source maps via loader query
            'css?sourceMap!' +
            'sass?sourceMap'
          ),
        },
      ],
    },
    plugins: [
      new cleanBuild(['dist']),
      new ExtractTextPlugin('main.scss'),
      new webpack.optimize.UglifyJsPlugin({
        output: {
          comments: false,
        },
        compress: {
          warnings: false,
        },
      }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
      }),
    ],
  };
} else {
  module.exports = {
    devtool: 'eval',
    entry: [
      'webpack-dev-server/client?http://localhost:3000',
      'webpack/hot/only-dev-server',
      './src/app.js',
    ],
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'index.min.js',
      publicPath: '/static/',
    },
    plugins: [
      new cleanBuild(['dist']),
      new webpack.HotModuleReplacementPlugin(),
      new ExtractTextPlugin('main.scss'),
    ],
    resolve: {
      extensions: ['', '.js', '.jsx', '.scss'],
    },
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules|bower_components)/,
          loader: ['babel'],
          query: {
            presets: ['react', 'es2015'],
          },
        },
        {
          test: /\.scss?$/,
          loader: 'style!css!sass',
          include: path.resolve(ROOT_PATH, 'src/styles'),
        },
      ],
    },
  };
}
