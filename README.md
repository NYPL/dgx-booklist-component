# BookList Component

This component is used to display a BookCover items using the [react-slick](https://github.com/akiran/react-slick) slider.

## Version
> v0.2.1

## Usage

> Require `dgx-booklist-component` as a dependency in your `package.json` file.

```sh
"dgx-booklist-component": "git+ssh://git@bitbucket.org/NYPL/dgx-booklist-component.git#master"
```

> Once installed, import the component in your React Application.

```sh
import BooklistWidget from 'dgx-booklist-component';
```

## Props

> You may initialize the component with the following properties:

```sh
<BooklistWidget
  id="BooklistWidget" ## String (default: 'BooklistWidget')
  className="BooklistWidget" ## String (default: 'BooklistWidget')
  lang="en" ## String representing the language (default: 'en')
  bookLists={[]} ## Array of book items
  errorMessage="Error message" ## String representing the error message if no items are available to iterate through
  slickResponsiveSettings={{}} ## Object containing the react-slick settings. See https://github.com/akiran/react-slick
  gaClickEvent={gaClickFunc()} ## Function for Google Analytics click events. The Action is determined by the gaActionText property and concatenated with the item number. The Label is the URL for each feature element (Optional)
  gaActionText="What's Happening" ## String representing the base of what the Google Analytics action field will be on each feature item
/>
```

## Local Development Setup
Run `npm install` to install all dependencies from your package.json file.

Next, startup the Webpack Development Server by running `npm start`. Visit `localhost:3000` in your web browser and begin coding.

> **NOTE:** We are currently using Webpack Hot Reload Server configuration which allows you to change your code without restarting your browser.

Once you have completed any code updates, ensure to run `npm run babel-build` in your terminal. This will build/bundle all your code in the `dist/` path via Webpack.
